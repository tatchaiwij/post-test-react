import { Component } from "react";
import UserCard from './component/Usercard'

class App extends Component {

  state = {
    userData: [],
  }

  componentDidMount() {
    this.FetchUser();
  }

  FetchUser = () => {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(data => this.setState({
      userData: data
    }));
  }

  render() {
    return (
      <div className="App" style={{margin : "20px"}}>
        <h2>User Lists</h2>
        {
          this.state.userData.map((user) => (
            <UserCard key={user.id} data={user}/>
          ))
        }
      </div>
    );
  }
}

export default App;
