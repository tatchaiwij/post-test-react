function Usercard(props) {

    const showUserData = () => {
        alert(JSON.stringify(props.data))
    }

    return(
        <div style={{borderStyle: "solid", width: "70vh", marginBottom: "20px", padding: "20px"}}>
            <p>ID: {props.data.id}</p>
            <p>Name: {props.data.name}</p>
            <p>Email: {props.data.email}</p>
            <p>Address: {props.data.address.street}  {props.data.address.suite}  {props.data.address.city}</p>
            <button onClick={() => showUserData()}>View More</button>
        </div>
    )
}

export default Usercard