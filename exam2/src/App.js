import Home from "./components/Home";
import User from "./components/User";
import UserMore from "./components/Usermore"

import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';

function App() {

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Navigate replace to="/home"/>} />
          <Route path='/home' element={<Home />} />
          <Route path='/user/:id' element={<User />} />
          <Route path='/user-more/:id' element={<UserMore />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}



export default App;
