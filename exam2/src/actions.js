export const addUser = user => {
  return {
    type: 'ADD_USER',
    id: user.id,
    name: user.name,
    email: user.email,
    address: `${user.address.street}  ${user.address.suite}  ${user.address.city}`,
    phone: user.phone,
    website: user.website,
    company: user.company.name
  }
}
