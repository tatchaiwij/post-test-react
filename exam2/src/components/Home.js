import { Component } from "react";

import * as actions from '../actions'
import { connect } from 'react-redux'
import { useEffect } from 'react';
import { Link } from 'react-router-dom';

function Home(props) {

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => add(data, props));
    }, [])


    return (
        <div>
            <h2>User Lists</h2>
            {
                props.userlist.map((user) => (
                    <Link to={{
                        pathname: `/user/${user.id}`,
                    }}>

                        <div style={{ borderStyle: "solid", width: "70vh", marginBottom: "20px", padding: "20px" }}>
                            <p>User: {user.name}</p>
                        </div>
                    </Link>
                ))
            }
        </div>
    );
}

const add = (data, props) => {
    {
        data.map((user) => (
            props.addUser(user)
        ))
    }
}

const mapStateToProps = (state) => ({
    userlist: state.userlist,
})

export default connect(mapStateToProps, actions)(Home)
