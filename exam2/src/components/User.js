import { useParams } from "react-router";
import Usercard from "./Usercard"
import { connect } from 'react-redux'

function User(props) {

    const { id } = useParams();

    return (
        <div style={{margin: "20px"}}>
            <h2>User Lists</h2>
            <Usercard data={props.userlist[id - 1]} />
        </div>
    )
}

const mapStateToProps = (state) => ({
    userlist: state.userlist
})


export default connect(mapStateToProps)(User)