import { Link } from 'react-router-dom';

function Usercard(props) {

    return (
        <div style={{ borderStyle: "solid", width: "70vh", marginBottom: "20px", padding: "20px" }}>
            <p>ID: {props.data.id}</p>
            <p>Name: {props.data.name}</p>
            <p>Email: {props.data.email}</p>
            <p>Address: {props.data.address}</p>
            <Link to={{
                pathname: `/user-more/${props.data.id}`,
                state: { data: props.data }
            }}> <button type="button">
                    View More
                </button>
            </Link>
        </div>
    )
}

export default Usercard