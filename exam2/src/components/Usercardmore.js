import { Link } from 'react-router-dom';

function Usercard(props) {

    return (
        <div style={{ borderStyle: "solid", width: "70vh", marginBottom: "20px", padding: "20px" }}>
            <p>ID: {props.data.id}</p>
            <p>Phone: {props.data.phone}</p>
            <p>Website: {props.data.website}</p>
            <p>Company: {props.data.company}</p>
            <Link to={{
                pathname: `/user/${props.data.id}`,
                state: { data: props.data }
            }}> <button type="button">
                    Back
                </button>
            </Link>
        </div>
    )
}

export default Usercard