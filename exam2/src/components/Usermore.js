import { useParams } from "react-router";
import Usermorecard from "./Usercardmore"
import { connect } from 'react-redux'

function Usermore(props) {

    const { id } = useParams();

    return (
        <div style={{margin: "20px"}}>
            <h2>User Lists</h2>
            <Usermorecard data={props.userlist[id - 1]} />
        </div>
    )
}

const mapStateToProps = (state) => ({
    userlist: state.userlist
})


export default connect(mapStateToProps)(Usermore)