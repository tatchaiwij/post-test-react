const initState = {
    userlist: [],
  }
  
  export default function rootReducer(state = initState, action) {
    switch (action.type) {
      case 'ADD_USER':
        return {
          ...state,
          userlist: [
            ...state.userlist,
            {
              id: action.id,
              name: action.name,
              email: action.email,
              address: action.address,
              phone: action.phone,
              website: action.website,
              company: action.company
            }
          ]
        }
      default:
        return state
    }
  }
  